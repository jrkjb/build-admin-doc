<br />
<div align="center">
    <img src="https://wonderful-code.gitee.io/images/readme/logo-title.png" alt="" />
    <br />
    <a href="https://demo.buildadmin.com/" target="_blank">演示站</a> |
    <a href="https://doc.buildadmin.com/" target="_blank">文档</a> |
    <a href="https://jq.qq.com/?_wv=1027&k=QwtXa14c" target="_blank">加群</a> |
    <a href="https://wonderful-code.gitee.io/guide/" target="_blank">视频介绍</a> |
    <a href="https://gitee.com/wonderful-code/buildadmin" target="_blank">Gitee仓库</a> |
    <a href="https://github.com/build-admin/BuildAdmin" target="_blank">GitHub仓库</a>
</div>
<br />
<p align="center">
    <a href="https://www.thinkphp.cn/" target="_blank">
        <img src="https://img.shields.io/badge/ThinkPHP-%3E6.0-brightgreen" alt="vue">
    </a>
    <a href="https://v3.vuejs.org/" target="_blank">
        <img src="https://img.shields.io/badge/Vue-3.x--Setup-brightgreen" alt="vue">
    </a>
    <a href="https://element-plus.gitee.io/#/zh-CN/component/changelog" target="_blank">
        <img src="https://img.shields.io/badge/Element--Plus-%3E2.1-brightgreen" alt="element plus">
    </a>
    <a href="https://www.tslang.cn/" target="_blank">
        <img src="https://img.shields.io/badge/TypeScript-%3E4.4-blue" alt="typescript">
    </a>
    <a href="https://vitejs.dev/" target="_blank">
        <img src="https://img.shields.io/badge/Vite-%3E2.0-blue" alt="vite">
    </a>
    <a href="https://vitejs.dev/" target="_blank">
        <img src="https://img.shields.io/badge/Pinia-%3E2.0-blue" alt="vite">
    </a>
    <a href="https://gitee.com/wonderful-code/buildadmin/blob/master/LICENSE" target="_blank">
        <img src="https://img.shields.io/badge/license-Apache2.0-blue" alt="license">
    </a>
</p>

<br>
<div align="center">
  <img src="https://wonderful-code.gitee.io/images/readme/index.gif" />
</div>
<br>

### 🌈 介绍
基于 Vue3.x setup + ThinkPHP + TypeScript + Vite + Pinia + Element Plus + vue-router-next，自适应多端、支持CRUD代码生成、自带WEB终端、同时提供Web和Server端、内置全局数据回收站和字段级数据修改保护、自动注册路由、无限子级权限管理等，无需授权即可免费商用，希望能帮助大家实现快速开发。

### 🎉 主要特性
- CRUD代码生成：一行命令生成管理代码，大气且实用的表格，多达22种表单组件支持，受控编辑和删除等。
- 内置终端：我们内置了一个WEB终端以实现一些理想中的功能，在不久的未来，您不再需要执行npm install和build命令。
- 顶级技术栈：除了基于Thinkphp6前后端分离架构外，我们的Vue3使用了Setup、状态管理使用Pinia等，流行且稳定。
- 提供字段级数据修改记录和修改对比；删除数据则自动统一回收，随时回滚和还原，安全且无感。
- 后台菜单规则管理：无限子级，自动注册路由，自由分派页面和按钮权限。
- 角色组/管理员/管理员日志
- 会员/会员组/会员余额、积分日志
- 系统配置/控制台/附件管理/个人资料管理
- 后续我们将持续开发各种oss、云短信等的支持，提供开箱即用的各行业完整应用。
- 更多特性等你探索...

### 💫 安装使用
我们提供了完善的文档，对于熟悉`ThinkPHP`和`Vue`的用户，请使用大佬版：[快速上手](https://wonderful-code.gitee.io/guide/install/start.html) ，对于新人朋友，我们额外准备了各个操作系统的从零开始套餐：[Windows从零到一](https://wonderful-code.gitee.io/guide/install/windows.html) | [Linux从零到一](https://wonderful-code.gitee.io/guide/install/linux-bt.html) | [MacBook安装引导](https://wonderful-code.gitee.io/guide/install/macBook.html)

### 📸 项目预览
|  |  |
|---------------------|---------------------|
|![登录](https://wonderful-code.gitee.io/images/readme/login.gif)|![控制台](https://wonderful-code.gitee.io/images/readme/dashboard.png)|
|![布局配置](https://wonderful-code.gitee.io/images/readme/layout.png)|![表格](https://wonderful-code.gitee.io/images/readme/admin.png)|
|![表单](https://wonderful-code.gitee.io/images/readme/user.png)|![系统配置](https://wonderful-code.gitee.io/images/readme/config.png)|
|![数据回收规则](https://wonderful-code.gitee.io/images/readme/data-recycle.png)|![数据回收日志](https://wonderful-code.gitee.io/images/readme/data-recycle-log.png)|
|![敏感数据](https://wonderful-code.gitee.io/images/readme/sensitive-data.png)|![菜单](https://wonderful-code.gitee.io/images/readme/menu.png)|
|![单栏布局](https://wonderful-code.gitee.io/images/readme/layout-3.png)|![经典布局](https://wonderful-code.gitee.io/images/readme/layout-2.png)|

### 🚀 联系我们
- [演示站 demo.buildadmin.com](https://demo.buildadmin.com/) 账户：`admin`，密码：`123456`（演示站数据无法修改，请下载源码安装体验全部功能）
- [文档：doc.buildadmin.com](https://doc.buildadmin.com/)
- [加群：687903819](https://jq.qq.com/?_wv=1027&k=QwtXa14c)
- [Gitee仓库](https://gitee.com/wonderful-code/buildadmin)
- [GitHub仓库](https://github.com/build-admin/BuildAdmin)
- [备用文档：wonderful-code.gitee.io](https://wonderful-code.gitee.io/)
- [邮箱 1094963513@qq.com](mailto:1094963513@qq.com)

### 💕 特别鸣谢
感谢巨人提供肩膀，排名不分先后
- [Thinkphp](http://www.thinkphp.cn/)
- [FastAdmin](https://gitee.com/karson/fastadmin)
- [Vue](https://github.com/vuejs/core)
- [Element Plus](https://github.com/element-plus/element-plus)
- [TypeScript](https://github.com/microsoft/TypeScript)
- [vue-router](https://github.com/vuejs/vue-router-next)
- [vite](https://github.com/vitejs/vite)
- [Pinia](https://github.com/vuejs/pinia)
- [Axios](https://github.com/axios/axios)
- [nprogress](https://github.com/rstacruz/nprogress)
- [screenfull](https://github.com/sindresorhus/screenfull.js)
- [mitt](https://github.com/developit/mitt)
- [sass](https://github.com/sass/sass)
- [wangEditor](https://github.com/wangeditor-team/wangEditor)
- [echarts](https://github.com/apache/echarts)
- [vueuse](https://github.com/vueuse/vueuse)
- [lodash](https://github.com/lodash/lodash)
- [eslint](https://github.com/eslint/eslint)
- [prettier](https://github.com/prettier/prettier)
- [vuepress](https://github.com/vuejs/vuepress)
- [countUp](https://github.com/inorganik/countUp.js)
- [Sortable](https://github.com/SortableJS/Sortable)
- [v-code-diff](https://github.com/Shimada666/v-code-diff)

### 🔐 版权信息
BuildAdmin 遵循`Apache2.0`开源协议发布，提供无需授权的免费使用。\
本项目包含的第三方源码和二进制文件之版权信息另行标注。

### 💕 支持项目

无需捐赠，如果觉得项目不错，或者已经在使用了，希望你可以去 [Github](https://github.com/build-admin/BuildAdmin) 或者 [Gitee](https://gitee.com/wonderful-code/buildadmin) 帮我们点个 ⭐ Star，这将是对我们极大的鼓励与支持。
