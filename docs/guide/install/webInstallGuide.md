---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

# 安装引导说明

> 本文对执行了`php think run`命令启动安装服务后，打开的[http://127.0.0.1:8000/](http://127.0.0.1:8000/)安装引导页面进行介绍。

- 在打开的弹窗中选择一个包管理器，若不理解，则随便选择一个即可
![](/images/web-install-guide/webInstallGuide1.jpg)
- 若您选择的包管理器还未安装，请点击进行安装
![](/images/web-install-guide/webInstallGuide2.jpg)
- 等待命令执行完毕
![](/images/web-install-guide/webInstallGuide3.jpg)
- 进入下一步，填写所有资料
- 若您之前已经在`phpStudy`添加数据库，其中`Mysql`相关资料则是您之前设置的数据库名称、用户、密码，而地址和端口号保持默认即可。
![](/images/web-install-guide/webInstallGuide4.jpg)
- 等待命令执行完毕，正常情况下会自动跳转到安装完成页面
![](/images/web-install-guide/webInstallGuide5.jpg)

若命令执行失败，跳转到了手动完成未尽事宜页面，请根据引导手动完成安装（您也可以点击右下角终端重试`安装依赖包`和`重新发布`），随后访问：[http://127.0.0.1:8000/index.html/#/admin](http://127.0.0.1:8000/index.html/#/admin)