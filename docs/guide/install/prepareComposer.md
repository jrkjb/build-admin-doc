---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

> <BaFullZip linkText="完整包" />通常无需安装`Composer`，Git克隆的包则必需安装

命令`composer -v`可以检查是否已安装过`Composer`，在一些Linux系统上，命令可能为`composer.phar -v`

### Windows系统
1. 下载并运行 [Composer-Setup.exe](https://getcomposer.org/Composer-Setup.exe)

### Linux/Mac OS系统请打开终端运行以下命令
``` bash
curl -sS https://getcomposer.org/installer | php mv composer.phar /usr/local/bin/composer
```
> 由于众所周知的原因，国外的网站连接速度很慢。因此安装的时间可能会比较长，我们建议使用国内镜像（阿里云）。

``` bash
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
```

> 如果遇到任何问题或者想更深入地学习 Composer，请参考Composer 文档（[英文文档](https://getcomposer.org/doc/)，[中文文档](http://www.kancloud.cn/thinkphp/composer)）。