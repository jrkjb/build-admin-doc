---
pageClass: max-content
---

# 安装服务目录或文件无写入权限

以`config`目录不可写为例

## Windows用户
1. 请确保是站点是使用[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)的方式打开的
2. 找到`buildadmin`项目下的`config`目录，检查是否具有读取和写入权限
3. 查看权限方式：右击`config`目录，点击属性，点击安全，选中您的计算机用户名
4. 页面上会展示您对该文件夹的权限列表
5. 要更改权限，请点击**编辑**\
![](/images/dirNoPermission1.png)
6. 在编辑窗口中，再次选中您的计算机用户名，勾选需要的权限保存即可

## Linux和Mac用户
1. 请确保是站点是使用[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)的方式打开的
2. 找到`buildadmin`项目下的`config`目录，检查目录的所有者，确定[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)的用户具有读取和写入权限
3. 修改权限命令`chmod 755 config`请在`buildadmin`目录执行；请注意文件所有者，如果文件所有者与启动安装服务的用户不一致，请参考第4点
4. 如果`3`不行，尝试改为使用`sudo php think run`命令[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)