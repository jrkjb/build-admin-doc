---
pageClass: max-content
---

# MacBook安装BuildAdmin

::: tip 提示
下文中若需查看大图，请右击图片，新标签打开
:::

## 必备软件
| 序号 | 软件名称 | 操作 | 备注 |
|:----:|:----:|:----:|:----:|
| 1 |MAMP|[下载](https://www.mamp.info/en/downloads/)|安装后请：[配置MAMP](https://wonderful-code.gitee.io/guide/install/macBook.html#MAMP配置)|
| 2 |NodeJs|[下载](https://nodejs.org/zh-cn/download/)|按引导完成安装即可|

## 准备开始
``` bash
# 切换到合适的目录(这里使用的是桌面)
cd ~/desktop

# 克隆项目
git clone https://gitee.com/wonderful-code/buildadmin.git

# 切换到项目目录
cd buildadmin

# 设置Composer源和下载PHP依赖包，完整包不需要执行这两条命令，git包是需要的
# 命令不存在请先完成MAMP配置
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
composer install

# 启动安装服务
sudo php think run
```
`php think run`命令执行成功后输出类似：
``` bash
ThinkPHP Development server is started On <http://0.0.0.0:8000/>
You can exit with `CTRL-C`
Document root is: D:\WWW\buildadmin\public
```
接下来，请在浏览器访问：[http://127.0.0.1:8000/](http://127.0.0.1:8000/)，根据引导完成安装即可，你也可以[继续查看安装引导说明](/guide/install/webInstallGuide.md)。

::: warning
在安装引导中，如遇WEB终端无法正常使用，请参考：[常见问题](https://wonderful-code.gitee.io/guide/install/start.html#常见问题)
:::

## MAMP配置
1. 按引导完成安装后打开软件
2. 点击左上角的`View Mode`显示出左侧菜单。
3. 切换到`PHP`菜单栏，切换默认`php版本`，并勾选如图所示的两个选项，保存。
- ![](/images/macbook/macbook1.png)
4. 切换到`MySql`菜单栏，勾选允许网络访问，并在右上角开启服务。
- ![](/images/macbook/macbook2.png)
5. 创建一个新的数据库（[如何创建？](/guide/install/macBook.html#创建数据库)）。

## 创建数据库
1. 打开`phpMyAdmin`
![](/images/macbook/macbook3.png)
2. 点击顶栏的`Databases`，然后在输入框自定义数据库名称，点击`创建`即可。
![](/images/macbook/macbook4.png)