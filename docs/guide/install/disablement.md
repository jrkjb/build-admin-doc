---
pageClass: max-content
---

# 解除PHP函数禁用

若您在解除函数时考虑到安全因素，请参考[安装服务进阶](https://wonderful-code.gitee.io/guide/install/senior.html)，安装服务使用正确不会造成任何潜在安全问题

### 以解除`popen`和`pclose`的禁用为例
1. 请先确保已将`PHP`加入到系统环境变量
2. 打开`终端`或`Windows PowerShell`
3. 执行`php --ini`命令，其中的`Loaded Configuration File:`所示文件，就是当前环境PHP加载的ini文件，**编辑它**
4. 搜索`disable_functions`
5. 比如您看到的是`disable_functions = system, exec, shell_exec, popen, pclose, passthru, proc_open, proc_close, proc_get_status,...`
6. 删除掉其中的`popen`、`pclose`并保存，修改后为`disable_functions = system, exec, shell_exec, passthru, proc_open, proc_close, proc_get_status,...`
7. 最后记得**重启安装服务**(启动服务的窗口Ctrl+c，再重新执行启动命令)