---
pageClass: max-content
---

# Windows从零搭建BuildAdmin

## 必备软件

| 序号 | 软件名称 | 下载链接 | 备注 |
|:----:|:----:|:----:|----|
| 1 |git|[下载](https://git-scm.com/downloads)|全程点击`Next`按钮即可完成安装|
| 2 |NodeJs|[下载](https://nodejs.org/zh-cn/)|全程点击`Next`按钮即可完成安装|
| 3 |phpStudy|[下载](https://www.xp.cn/download.html)|[phpStudy安装与配置](/guide/install/windows.md#phpstudy安装与配置)|
| 4 |Composer|[下载](https://getcomposer.org/Composer-Setup.exe)|Git克隆的包必需安装`Composer`，<BaFullZip linkText="完整包" />可选，[Composer安装](/guide/install/windows.md#composer安装)|

## 准备开始
打开命令行工具（按下`Win+R`，选择`Windows PowerShell`），请参考注释执行下列命令
``` bash
# 克隆项目
git clone https://gitee.com/wonderful-code/buildadmin.git

# 切换到项目目录
cd buildadmin

# 设置Composer源和下载PHP依赖包，完整包不需要执行这两条命令，git包是需要的
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
composer install

# 启动安装服务
php think run
```
`php think run`命令执行成功后输出类似：
``` bash
ThinkPHP Development server is started On <http://0.0.0.0:8000/>
You can exit with `CTRL-C`
Document root is: D:\WWW\buildadmin\public
```
接下来，请在浏览器访问：[http://127.0.0.1:8000/](http://127.0.0.1:8000/)，根据引导完成安装即可，你也可以[继续查看安装引导说明](/guide/install/webInstallGuide.md)。

::: warning
在安装引导中，如遇WEB终端无法正常使用，请参考：[常见问题](https://wonderful-code.gitee.io/guide/install/start.html#常见问题)
:::

**以下无安装流程步骤**

## phpStudy安装与配置
1. 直接点击`立即安装`完成安装后打开软件
2. 在软件**首页**，启动`Mysql`服务\
![](/images/windows/phpStudy1.png)
3. 在软件**数据库**`修改root密码`和`创建数据库`\
\
![](/images/windows/phpStudy2.png)
- 数据库名、用户名、密码均为随意填写，安装`BuildAdmin`时用得上\
\
![](/images/windows/phpStudy3.png)

## Composer安装
1. 运行`Composer`安装程序
2. 在安装程序的第二步，从`phpStudy`的安装目录中，选择`php.exe`文件的路径，以及勾选`添加到环境变量`\
![](/images/windows/Composer1.png)
3. 一直`下一步`完成安装即可
4. 执行`composer -v`命令，检查是否已准备好\
![](/images/windows/Composer2.png)