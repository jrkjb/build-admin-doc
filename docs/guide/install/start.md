---
pageClass: max-content
---

# 前序准备
| 序号 | 描述 | 操作 |
|:----:|----|----|
| 1 | 下载<BaFullZip linkText="完整包" />或克隆`BuildAdmin`代码到本地 |  |
| 2 | PHP >= 7.1 且 &lt;= 7.3 (开发环境为PHP7.1版本) | [达成指引](https://wonderful-code.gitee.io/guide/install/preparePHP.html) |
| 3 | Mysql >= 5.6 且 &lt;= 8.0 (需支持innodb引擎、开发环境为5.7版本) |    |
| 4 | NodeJs >= 14.13.1 | [达成指引](https://wonderful-code.gitee.io/guide/install/prepareNodeJs.html) |
| 5 | Npm >= 7.0.0 | [达成指引](https://wonderful-code.gitee.io/guide/install/prepareNpm.html) |
| 6 | Composer（<BaFullZip linkText="完整包" />不必要，Git克隆包必需安装） | [达成指引](https://wonderful-code.gitee.io/guide/install/prepareComposer.html) |

# 启动安装服务
``` bash
cd buildadmin
# 其中 buildadmin 为项目根目录，该目录包含一个没有前后缀的 think 文件

composer install
# 完整包不需要执行这条命令，若找不到命令，可以尝试:composer.phar install

php think run
# Linux下推荐使用:sudo php think run
# Linux下若sudo后仍然异常，请确保 buildadmin 目录的所有者和执行此命令的用户一致，推荐root
```
上述命令执行成功后输出类似：
``` bash
ThinkPHP Development server is started On <http://0.0.0.0:8000/>
You can exit with `CTRL-C`
Document root is: D:\WWW\buildadmin\public
```
接下来，请在浏览器访问：[http://127.0.0.1:8000/](http://127.0.0.1:8000/)，根据引导完成安装即可
::: tip 提示
若您**无法自主完成安装**，请移步更详细的[Windows下完整安装流程](/guide/install/windows.md)或[Linux下完整安装流程](/guide/install/linux-bt.md)也有[MacBook下安装引导](/guide/install/macBook.md)
:::

::: warning 提示
1. 安装完成后，请您一定查看[开发者必看](https://wonderful-code.gitee.io/guide/other/developerMustSee.html)文档。
2. 安装服务只提供WEB服务，其他比如`MySQL`数据库的服务需要自行单独管理。
3. 安装完成后推荐使用`Nginx、Apache`等服务器软件运行站点，站点的根目录为`buildadmin`目录，站点运行目录为`buildadmin/public`，无需配置隐藏`index.php`，可以选择配置：[隐藏index.html](https://wonderful-code.gitee.io/guide/install/hideIndex.html)
:::

# 停止安装服务
1. 在`命令行窗口`，按下`Ctrl+C`，即可停止服务。
2. 在`命令行窗口程序`停止运行/断开时，服务会自动停止。
3. 停止服务后，站点将不能继续访问，通常应使用`Nginx`、`Apache`等服务器软件来搭建您的站点。
4. 后续需要使用安装服务时，再启动即可

# 常见问题
1. 命令`php -v`可以查看当前环境PHP版本，请确保`PHP版本>=7.1`，若无`PHP`命令，请先[安装PHP](https://wonderful-code.gitee.io/guide/install/preparePHP.html)，并将PHP加入环境变量
2. 请确定在站点的根目录执行`php think run`命令，也就是`think`文件所在目录，请注意该文件无任何后缀
3. 检查您打开的站点域名，确定是打开了[安装服务](https://wonderful-code.gitee.io/guide/install/start.html)的`IP/域名:8000`站点，而非是运行于`Nginx、Apache`服务下的站点。

#### 安装服务无法访问/WEB终端无法连接
请检查`buildadmin`站点目录的权限和用户组，同时请检查您打开的站点域名(以上第3点)。

#### 提示`xxxxx() has been disabled for security reasons`
表示`xxxxx`所示的函数，在`php.ini`中被禁用了，[请参考这里](https://wonderful-code.gitee.io/guide/install/disablement.html)，解除函数禁用

#### 提示`composer install` 命令不存在？
如果您已完成了`Composer`的安装，但还是找不到`composer`命令，可以尝试`composer.phar install`命令，如果命令还不存在，那么请检查您电脑的环境变量设置，问题无法解决时，请使用：<BaFullZip linkText="完整包" />

#### WEB终端提示`xxxxx: command not found`
请检查您打开的站点域名(以上第3点)，然后`Linux`用户请尝试使用`sudo php think run`来启动服务。

#### WEB终端提示`权限不足`、`permission denied`
请检查您打开的站点域名(以上第3点)，然后`Linux`用户请尝试使用`sudo php think run`来启动服务，还不行，请检查`buildadmin`目录权限和用户组。