---
pageClass: max-content
---

> 系统安装成功后，URL通常为`您的域名/index.html/#/`，此文档可以引导您隐藏掉URL中的`index.html`，ps：不隐藏并不会影响站点，除非您认为它不好看

我们需要修改站点的**默认文档**，将`index.html`放在第一位，以下根据不同的服务器环境进行介绍

## 宝塔面板
1. 登录面板
2. 侧边栏打开`网站`菜单，找到`BuildAdmin`对应的站点
3. `设置`站点信息，在打开的窗口中点击`默认文档`
4. 将`index.html`放到第一行，保存(添加)即可
5. 您已经可以使用不含`index.html`的URL来访问站点了
![](/images/hideIndex1.png)

## phpStudy
1. 打开`phpStudy`
2. 侧边栏打开`网站`菜单，找到`BuildAdmin`对应的站点
3. `管理`站点信息，在打开的弹窗中点击`网站首页设置`
4. 以原格式，将`index.html`放到第一位，保存即可
![](/images/hideIndex2.png)

## Nginx
1. 找到`Nginx`安装目录
2. `安装目录/conf/vhosts`文件夹内，找到`BuildAdmin`对应站点的配置文件，编辑它
```
// 找到其中的
location / {
    index index.php index.html error/index.html;
    ......
}
修改为：（将index.html放到第一位）
location / {
    index index.html index.php error/index.html;
    ......
}
```
3. 配置文件路径仅供参考，不同版本的`Nginx`可能不一样
4. 配置内容仅供参考，主要是找`index`开头那一行
