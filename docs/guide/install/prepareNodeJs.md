---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

## 未安装`NodeJs`
### Windows系统
- 直接安装`NodeJs`，[下载地址](https://nodejs.org/zh-cn/)
- 使用`下一步`安装法即可
- 安装完成后打开`cmd`或`Windows PowerShell`
- 运行命令`node -v`出现版本号则代表安装成功，如果没有请检查系统环境变量
- 运行命令`npm install -g npm`对`npm`进行升级，因为自带的版本很低

### Linux系统
```
系统教多，暂略...
建议您直接搜索如：Ubuntu安装nodejs
```

## 已安装`NodeJs`但需要升级
- 如果您当前环境下的`node.js`版本没有达到要求
- 首先安装`n`模块，执行命令 **npm install -g n --force**
- 升级`node.js`到最新稳定版本，执行命令`n stable`
- 升级`node.js`到指定版本，执行命令`n v16.13.0`
- 同时建议[升级NPM](https://wonderful-code.gitee.io/guide/install/prepareNpm.html)