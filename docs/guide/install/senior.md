---
pageClass: max-content
---

# 指定安装服务端口
1. 请确保要使用的端口没有被占用
2. 启动服务执行`php think run -p 端口号`命令
3. 比如`80`没有被占用情况下，那么可以执行命令`php think run -p 80`，浏览器访问[http://localhost/](http://localhost/)
4. 比如`1888`没有被占用情况下，那么可以执行命令`php think run -p 1888`，浏览器访问[http://localhost:1888/](http://localhost:1888/)

# 指定安装服务地址
支持指定服务地址，如`php think run -H ba.com -p 80`\
会显示：
``` bash
ThinkPHP Development server is started On <http://ba.com:80/>
You can exit with `CTRL-C`
Document root is: D:\WWW\ba\public
```
然后你就可以直接在浏览器访问：`http://ba.com/`

# 安装服务命令限制
您可以对WEB终端的命令进行限制。\
在`/config/buildadmin.php`文件中的`allowed_commands`字段上，`BuildAdmin`预设了一系列允许执行命令，这些命令都是非常安全的；如果您想通过安装服务执行其他的命令，请自行向该字段追加

# 服务安全保障
在`终端`，按下`Ctrl+C`，即可停止服务；在`终端`停止运行/断开时，服务会自动停止；加上以上的**指定安装服务端口**和**安装服务命令限制**，安装服务使用正确不会造成任何潜在安全问题。

::: warning 提示
1. 安装完成后，请您一定查看[开发者必看](https://wonderful-code.gitee.io/guide/other/developerMustSee.html)文档。
2. 安装服务只提供WEB服务，其他比如`MySQL`数据库的服务需要自行单独管理。
3. 安装完成后推荐使用`Nginx、Apache`等服务器软件运行站点，站点的根目录为`buildadmin`目录，站点运行目录为`buildadmin/public`，无需配置隐藏`index.php`，可以选择配置：[隐藏index.html](https://wonderful-code.gitee.io/guide/install/hideIndex.html)
:::