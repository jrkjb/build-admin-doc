---
pageClass: max-content
---

> PHP没有文件操作权限，通常指的是`feof`和`fgets`函数在`php.ini`中被禁用掉了

### 解除函数禁用
- 打开`终端`或`Windows PowerShell`
- 执行`php --ini`命令，其中的`Loaded Configuration File:`所示文件，就是当前环境PHP加载的ini文件，编辑它
- 搜索`disable_functions`
- 比如您看到的是`disable_functions = system, exec, shell_exec, feof, fgets, passthru, proc_open, proc_close, proc_get_status,...`
- 删除掉其中的`feof`、`fgets`并保存，修改后为`disable_functions = system, exec, shell_exec, passthru, proc_open, proc_close, proc_get_status,...`
- 最后记得**重启安装服务**(启动服务的窗口Ctrl+c,再重新执行启动命令)