---
pageClass: max-content
---

::: tip
本文档为安装引导附录，若您**已经**完成了系统的安装，无需阅读此文档，请移步[进阶](https://wonderful-code.gitee.io/senior/)。
:::

### 设置NPM源失败
设置一个合适的NPM源可以加快`npm install`的执行过程，若安装程序自动设置源失败，您可以手动执行以下命令之一来完成源的设置：
``` bash
# 淘宝源
npm config set registry https://registry.npm.taobao.org/

# rednpm源
npm config set registry http://registry.mirror.cqupt.edu.cn/

# npm源
npm config set registry https://registry.npmjs.org/
```