---
pageClass: max-content
---

# Linux从零搭建BuildAdmin

::: tip 提示
系统信息：CentOS stream 9 64位\
以全新系统，通过宝塔面板从零搭建为例\
下文中若需查看大图，请右击图片，新标签打开
:::

## 宝塔面板的安装
1. 使用shell工具连接服务器
2. [在宝塔官方网站页面](https://www.bt.cn/new/download.html)，找到`CentOS`系统安装面板所需的命令，直接复制执行。
3. 耐心等待命令执行成功后，可以看到如下信息：\
![](/images/linux-bt/linux-bt1.png)
4. 浏览器内打开`外网面板地址`，并通过`username`和`password`进行登录。
5. 进入面板后，会要求绑定宝塔官网账户，请自行注册和绑定即可。
6. 接下来，面板会自动弹出如下窗口（截图所示为推荐的版本号）：
![](/images/linux-bt/linux-bt2.png)
7. `Nginx`选择最新版本、`MySQL >= 5.7`版本、`PHP >= 7.1 且 <= 7.3`版本；极速安装与编译安装均可。
8. 正常情况下，请耐心等待所有软件安装完成，极速安装约30分钟，不同配置的机器，所需时间不定。
9. 如果您的服务器任有余力，您可以在`shell`工具上，同时进行[NodeJs的安装](https://wonderful-code.gitee.io/guide/install/linux-bt.html#nodejs的安装)和[git的安装](https://wonderful-code.gitee.io/guide/install/linux-bt.html#git的安装)

## 开放端口
在宝塔面板侧边菜单打开`安全`，放行`8000`号端口，后续安装服务会用得上。
![](/images/linux-bt/linux-bt7.png)

## 解除PHP函数禁用
开始此步骤前，需等待宝塔面板左上角的`PHP`安装任务已完成
1. 在宝塔面板侧边菜单打开`软件商店`，在商店中找到安装好的`PHP`，点击`设置`
![](/images/linux-bt/linux-bt3.png)
2. 在打开的设置页面中点击`禁用函数`，删除`putenv,proc_open,popen,passthru`这**4个**函数的禁用。
![](/images/linux-bt/linux-bt4.png)

## 建立数据库
开始此步骤前，需等待宝塔面板左上角的所有安装任务已完成
#### 建立方案一
在宝塔侧边菜单中点击`网站->添加网站`，填写`域名`同时`创建MySql`
![](/images/linux-bt/linux-bt6.png)
创建的站点，可用于后续正式部署

#### 建立方案二
在宝塔侧边菜单中点击`数据库->添加数据库`，数据库名称与用户名自定义即可
![](/images/linux-bt/linux-bt5.png)

## NodeJs的安装
``` bash
sudo yum install epel-release
sudo yum install nodejs
node -v
```

## git的安装
``` bash
# CentOS的git安装命令，直接执行即可
yum -y install git
git --version
```
其他系统请[在这里](https://git-scm.com/download/linux)，查找对应的安装命令。

## 准备开始安装BuildAdmin
``` bash
# 切换到合适的目录(这里使用的是宝塔站点专用目录)
cd /www/wwwroot

# 克隆项目
git clone https://gitee.com/wonderful-code/buildadmin.git

# 切换到项目目录
cd buildadmin

# 设置Composer源和下载PHP依赖包，完整包不需要执行这两条命令，git包是需要的
composer config -g repo.packagist composer https://mirrors.aliyun.com/composer/
composer install

# 启动安装服务（非root）
sudo php think run

# 启动安装服务（root用户）
php think run
```
`php think run`命令执行成功后输出类似：
``` bash
ThinkPHP Development server is started On <http://0.0.0.0:8000/>
You can exit with `CTRL-C`
Document root is: D:\WWW\buildadmin\public
```
接下来，请在浏览器访问：`http://IP:8000/`，请注意是带端口IP/域名，非`Nginx`提供服务的域名，根据引导完成安装即可，你也可以[继续查看安装引导说明](/guide/install/webInstallGuide.md)。

::: warning
在安装引导中，如遇WEB终端无法正常使用，请参考：[常见问题](https://wonderful-code.gitee.io/guide/install/start.html#常见问题)
:::