---
pageClass: max-content
---

# 开发者必看
::: tip
若您已经安装好系统，并准备开始二次开发或体验CRUD、终端等对代码会有变更的操作，我们建议您首先阅读本文档。
:::

### 开发环境
1. 建议您在本地PC上安装好 BuildAdmin 系统，作为开发环境。
2. 建议您全程使用`php think run`命令启动的服务来进行开发工作，可以选择**不开启**`Nginx、Apache`之类的服务器软件，参[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#%E5%90%AF%E5%8A%A8%E5%AE%89%E8%A3%85%E6%9C%8D%E5%8A%A1)
3. 在安装 BuildAdmin 时您已经填写了系统的数据库资料，需要开启对应的数据库服务，数据库资料被保存在`config/database.php`文件。
4. 在`/web`目录内，执行`npm run dev`命令，在浏览器打开[localhost:1818](http://localhost:1818/)。
5. 开发时，建议开启TP框架的调试模式：找到网站根目录的`.env-example`重命名为`.env`。参：[开启调试模式](https://wonderful-code.gitee.io/senior/server/debug.html)。

以上五步曲之后，您修改前端代码，`localhost:1818`的页面会热更新，方便您实时调试。并且api请求会有具体报错信息，CRUD代码生成后，立马就可以看到效果等，接下来，您可以开始查阅[**进阶文档**](https://wonderful-code.gitee.io/senior/)。

### 线上环境
1. 建议删除`/install`目录。
2. 线上环境可以选择**不上传**`/web`目录，前端每次重新发布后，只将`/public/assets 目录`和`/public/index.html 文件`，同步到服务器上即可。
3. 使用`Nginx、Apache`等服务器软件运行站点，站点的根目录配置为`buildadmin`目录，站点运行目录为`buildadmin/public`，无需配置隐藏`index.php`，可以选择配置：[隐藏index.html](https://wonderful-code.gitee.io/guide/install/hideIndex.html)。