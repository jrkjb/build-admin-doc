> BuildAdmin是一个持续迭代版本、新增功能和修复BUG的系统，您可以参考此文档保持您的BuildAdmin为最新版本。

## Git包
我们推荐您使用Git管理系统的代码，这是目前世界上最好的版本管理工具，没有之一。

#### 更新核心代码
``` bash
# 保存工作现场（将目前还不想提交的但是已经修改的代码保存至堆栈中）
git stash

# 从远程仓库获取最新代码并自动合并到本地
git pull

# 恢复工作现场
git stash pop
```
<!-- 在执行`git pull`命令时，Git会尽可能的自动完成代码的合并、并重建提交树等，有时您在本地的二次开发可能会造成代码冲突 -->

#### 更新后端依赖
``` bash
composer update -vvv
```

#### 更新数据库
如果`app/admin/buildadmin.sql`文件有更新，请根据`Git`更新日志对比直接修改数据库。

#### 清理系统缓存
后台->右上角垃圾桶图标->清理系统缓存（该操作会删除`runtime/cache目录`）

#### 更新时额外可能会用到的命令
``` bash
# 查看远程仓库信息
git remote -v

# 查看 stash 队列
git stash list

# 清空 stash 队列
git stash clear
```