---
pageClass: max-content
---

# 介绍
## 项目介绍
🔥🔥项目是基于[Thinkphp6](https://www.thinkphp.cn/)、[Vue3](https://v3.cn.vuejs.org/)、[TypeScript](https://www.tslang.cn/docs/home.html)、[Vite](https://vitejs.cn/)、[Element Plus](https://element-plus.gitee.io/zh-CN/#/zh-CN)、[Pinia](https://pinia.vuejs.org/)等的后台管理系统，详细介绍请[查看首页](https://wonderful-code.gitee.io/)，数据库使用[Mysql](https://www.mysql.com/)，并支持[国际化i18n](https://vue-i18n.intlify.dev/)。此项目永久免费开源，无需授权即可商业使用。

## 快速了解
- 什么是内置终端\
<iframe class="bilibili-iframe" src="//player.bilibili.com/player.html?aid=982614044&bvid=BV12t4y1h7zW&cid=748475843&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"></iframe>

- 什么是CRUD\
<iframe class="bilibili-iframe" src="//player.bilibili.com/player.html?aid=597505755&bvid=BV1YB4y1S7Yj&cid=748467729&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"></iframe>

## 在线预览
[点击前往演示站：https://demo.buildadmin.com](https://demo.buildadmin.com/)\
账户：admin\
密码：123456
::: warning 提示
演示站已禁用编辑和删除数据权限，请下载源码安装体验所有功能。
:::

## 代码版本与下载
#### Git仓库分支
- `master`含项目完整的`Web端、Server端`以及已经编译好的`安装器`代码，需要手动执行`composer install`。
- `develop`当前正在开发的版本。
- `install`系统安装器的源代码，已经编译到`master`分支的`install`目录，用于系统安装，此分支代码与系统运行无关。
- `web`纯Web端的源代码(尚未开始整合接口之前)，在整合Server端之后就没在维护了。

#### 其他
- 完整包<BaFullZip />：已经进行了`composer install`，这样您就不再需要安装`composer`了，但仍然需要根据安装程序引导执行`npm install`等。
- 资源包<BaResourceZip />：仅含`Composer`等资源，用于直接覆盖到项目根目录，不含核心代码。

::: tip 代码包的选择
1. 如果您是纯**前端开发者**，并且不了解`Composer`，建议直接使用**完整包**，下载后可以执行一下`git pull`命令获取最新修改。
2. 如果您是**后端、全栈**开发者，就算不了解`Composer`，也建议您使用**Git包**（参考文档进行安装即可），它可以帮助您搭建好完整的开发环境，方便后续二次的开发。
3. 如果您希望方便快速体验系统，请直接使用**完整包**。
:::