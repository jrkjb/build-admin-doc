::: tip FastAdmin
他是一款同样支持CRUD的后台，并已有大量开箱即用的完整应用可以直接安装使用，非常值得了解。\[ [官网](https://www.fastadmin.net/) | [体验](https://www.fastadmin.net/demo.html) | [Gitee](https://gitee.com/karson/fastadmin) \]。
:::

本项目灵感起自于[FastAdmin交流群](https://jq.qq.com/?_wv=1027&k=54I6mts)，并在规划阶段多次和`FastAdmin`创始人`Karson`沟通，本项目亦有不少地方照搬于`FastAdmin`(当然还是有很多原创的)，在此特别感谢大佬的无私奉献和支持。
- [点击拥有FastAdmin](https://gitee.com/karson/fastadmin)
- [点击拥有大量开箱即用的完整应用](https://www.fastadmin.net/store.html)
- [基于TP+FA+Swoole的企业Im客服系统](https://www.fastadmin.net/store/fastim.html)也非常nice


> 本站仅此一份第三方的广告页面/链接，意在鸣谢，您不会看到任何其他广告。