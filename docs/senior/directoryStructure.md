---
pageClass: max-content
---

# 目录结构

## Server 端

```
Server 端
使用 Thinkphp6 多应用推荐目录结构
├─app（应用目录）
│  ├─admin
│  ├─api
│  └─common
│
├─config（配置目录）
│
├─extend（扩展目录）
│
├─install（安装器源代码，此目录不一定存在）
│
├─public（运行目录）
│  ├─npm-install-test（npm install测试项目）
│  ├─static
│  │  ├─fonts（一些字体）
│  │  │
│  │  └─images（一些图片）
│  │
│  └─storage（上传的文件保存在这里）
│
├─runtime（运行时目录，缓存、日志等）
├─web（WEB端源代码，见下文详叙）
│  .env-example（env配置示例）
│  .gitattributes
│  .gitignore
│  .travis.yml
│  CHANGELOG.md（更新日志）
│  composer.json
│  README.md
└─ think（命令行入口文件）

```
更多细节，您还可以参考[TP文档中的目录结构](https://www.kancloud.cn/manual/thinkphp6_0/1037483)，请翻阅**多应用模式**

## web 端

```
web 端
使用经典目录结构设计，并合理优化
├─public（公共文件）
├─src
│  │  App.vue
│  │  main.ts
│  │
│  ├─api（所有接口相关）
│  │  │  common.ts（公共Url定义和公共请求方法）
│  │  │  controllerUrls.ts（控制器Url定义）
│  │  │
│  │  ├─backend（后台接口方法定义）
│  │  │
│  │  └─frontend（前台接口方法定义）
│  │
│  ├─assets（静态资源）
│  │
│  ├─components（组件）
│  │  ├─baInput（输入组件封装：常用+图片/文件上传、数组、城市/图标选择等封装在了一个组件内）
│  │  │
│  │  ├─contextmenu（tabs右击菜单）
│  │  │
│  │  ├─editor（富文本编辑器）
│  │  │
│  │  ├─formItem（表单项，结合baInput）
│  │  │
│  │  ├─icon（同时支持图标组件，支持加载本地svg、阿里、element、awesome）
│  │  │
│  │  ├─table（表格封装）
│  │  │
│  │  └─terminal（终端）
│  │
│  ├─lang（所有语言包）
│  │  │  globs-en.ts（公共英文语言包）
│  │  │  globs-zh-cn.ts（公共中文语言包）
│  │  │
│  │  └─pages（页面语言包：`t('pageName.翻译名')`进行调用）
│  │
│  ├─layouts（布局）
│  │  ├─backend（后台布局）
│  │  │  components （布局组件）
│  │  │  container （布局容器组件，内含默认的三种布局方案）
│  │  │  index.vue （后台布局入口）
│  │  ├─frontend（前台布局）
│  │  │  components （布局组件）
│  │  │  container （布局容器组件，内含默认的布局方案）
│  │  │  user.vue （会员中心布局入口）
│  │  └─common（公共布局组件）
│  │
│  ├─router（路由）
│  │      static.ts（静态路由配置）
│  │
│  ├─stores（状态商店）
│  │  ├─adminInfo.ts（管理员资料）
│  │  ├─config.ts（布局相关）
│  │  ├─memberCenter.ts（会员中心）
│  │  ├─navTabs.ts（后台顶栏tab）
│  │  ├─siteConfig.ts（站点配置）
│  │  ├─terminal.ts（终端）
│  │  ├─userInfo.ts（用户资料）
│  │  │
│  │  ├─constant（常量定义）
│  │  │      cacheKey.ts（缓存Key）
│  │  │
│  │  └─interface（接口定义）
│  │          index.ts
│  │
│  ├─styles（样式表）
│  │      app.scss
│  │      base.scss
│  │      element.scss（element的css覆盖）
│  │      loading.scss
│  │      var.scss（css变量定义）
│  │
│  ├─utils（工具库）
│  │      axios.ts（网络请求封装）
│  │      baTable.ts（表格封装）
│  │      build.ts（构建时）
│  │      common.ts（公共方法）
│  │      directives.ts（指令）
│  │      horizontalScroll.ts（横向滚动工具）
│  │      iconfont.ts（图标）
│  │      layout.ts（布局辅助）
│  │      loading.ts（页面加载）
│  │      pageBubble.ts（页面气泡效果）
│  │      pageShade.ts（全局阴影）
│  │      random.ts（随机数、字符串生成）
│  │      router.ts（路由辅助）
│  │      storage.ts（local缓存、session缓存封装）
│  │      useCurrentInstance.ts（快速获取当前实例）
│  │      validate.ts（一些表单验证方法）
│  │      vite.ts（vite运行与编译时）
│  │
│  └─views（视图）
│      ├─backend（后台视图）
│      ├─common（公共视图）
│      └─frontend（前台视图）
│
├─types
│  .editorconfig（IDE风格统一配置）
│  .env（基础环境变量定义）
│  .env.development（开发环境变量定义）
│  .env.production（生产环境变量定义）
│  .eslintignore（eslint忽略）
│  .eslintrc.js（eslint配置）
│  .prettierrc.js（prettier配置）
│  index.html（入口文件）
│  package.json
│  README.md
│  tsconfig.json（ts配置）
└─ vite.config.ts（vite配置）
```
