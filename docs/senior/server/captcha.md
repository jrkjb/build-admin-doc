---
pageClass: max-content
---

# 验证码

我们准备了`ba\Captcha`类，该类基于`TP`的验证码类改造而成：通过Mysql保存验证码而不是Session以更好的支持API访问。

使用验证码类的方法如下：
1. 前端生成一个唯一的`id`。
2. 携带`id`请求验证码生成接口，该接口应直接输出图片，比如：`/index.php/api/common/captcha`。
3. 提交表单时，携带`id`，以对验证码的正确性进行验证。

#### 验证码生成示例
``` php
// 接受ID
$captchaId = $this->request->request('id');

// 一些验证码的基本配置
$config    = array(
    'codeSet'  => '123456789',            // 验证码字符集合
    'fontSize' => 22,                     // 验证码字体大小(px)
    'useCurve' => false,                  // 是否画混淆曲线
    'useNoise' => true,                   // 是否添加杂点
    'length'   => 4,                      // 验证码位数
    'bg'       => array(255, 255, 255),   // 背景颜色
);

// 实例化验证码类，传递`ID`并输出验证码
$captcha = new Captcha($config);
return $captcha->entry($captchaId);
```

#### 验证码验证示例
``` php
// 接受验证码和验证码ID
$data['captcha']   = $this->request->post('captcha');
$data['captchaId'] = $this->request->post('captcha_id');

$captchaObj = new Captcha();
if (!$captchaObj->check($data['captcha'], $data['captchaId'])) {
    $this->error('验证码错误！');
}
```

## 配置
验证码类带有默认的配置参数，也支持自定义配置，这些配置项包括：
|配置项|注释|默认值|
|:----:|----|----|
|codeSet|验证码字符串集合|数字、大小写字母，(以去除`1l0o`等容易混淆的字符)|
|expire|验证码过期时间(秒)|600秒(10分钟)|
|useZh|使用中文验证码|false|
|zhSet|中文验证码字符串|一些预设的汉字|
|useImgBg|使用图片背景|false|
|fontSize|验证码字体大小(px)|25(px)|
|useCurve|是否画混淆曲线|true|
|useNoise|是否添加杂点|true|
|imageH|验证码图片高度|0:自动计算|
|imageW|验证码图片宽度|0:自动计算|
|length|验证码位数|验证码位数|
|fontttf|验证码字体，不设置则随机获取||
|bg|验证码背景色|rgb[243, 251, 254]|
|reset|验证成功后重置|true|