---
pageClass: max-content
---

# 控制器

> 阅读此文档，需要对`ThinkPHP`有基本的了解，[TP控制器文档](https://www.kancloud.cn/manual/thinkphp6_0/1037510)

- 我们在`app\common\controller`准备了`Backend`（后台基类）和`Frontend`（前台基类），系统几乎所有的控制器都是继承于它们的。
- 而前后台基类都是继承于`Api`类，在`Api`类中我们根据路由加载了语言包等，同时定义了以下快捷方法：

## 快捷方法
#### 操作成功
``` php
success($msg = '', $data = null, $code = 1, $type = null, $header = [], $options = []);
```

- `$msg`操作成功的提示消息
- `$data`返回数据
- `$code`状态码
- `$type`输出类型：json、jsonp、xml、response=输出HTML、view=渲染模板输出、redirect=页面重定向、download=附件下载
- `$header`发送的`Header`头
- `$options`发送的额外参数

##### 使用示例
``` php
$this->success('操作成功~');
$this->success('操作成功~', [
    'title'   => '标题',
    'content' => '内容',
]);
```

#### 操作失败
此方法与`success()`方法只有`code`是不一样的。
``` php
error($msg = '', $data = null, $code = 0, $type = null, array $header = [], $options = []);
```

#### 返回数据
`success()`和`error()`方法都是基于它进行封装的。
``` php
result($msg, $data = null, $code = 0, $type = null, $header = [], $options = []);
```

## 后台基类
后台基类`app\common\controller\Backend`，后台的所有控制器，都是继承于它的，它已实现以下功能：
- 自动根据`token`完成管理员的登录和鉴权。
- 构建和组装查询`SQL`，比如前台发送的快速搜索、公共搜索、排序等操作，都是由此类下的`queryBuilder()`方法构建的查询`SQL`。
- 引入了`Trait`类来实现`查询`、`添加`、`编辑`等功能。

#### 后台基类属性
父类可以直接重写这些属性。
```php
/**
 * 无需登录的方法
 * 访问本控制器的此方法，无需管理员登录
 */
protected $noNeedLogin      = ['index', 'login'];

/**
 * 无需鉴权的方法
 */
protected $noNeedPermission = ['test'];

/**
 * 新增/编辑时，对前端发送的字段进行排除（忽略不入库）
 */
protected $preExcludeFields = ['createtime', 'updatetime'];

/**
 * 权重字段，拖拽排序依据字段
 */
protected $weighField = 'weigh';

/**
 * 在查看数据时的：默认的排序字段
 */
protected $defaultSortField = 'id,desc';

/**
 * 快速搜索字段
 */
protected $quickSearchField = 'id';

/**
 * 是否开启模型验证
 */
protected $modelValidate = true;

/**
 * 是否开启模型场景验证
 */
protected $modelSceneValidate = false;

/**
 * 关联查询方法名
 * 方法应定义在模型中
 */
protected $withJoinTable = [];

/**
 * 关联查询JOIN方式
 */
protected $withJoinType = 'LEFT';

/**
 * 权限类实例
 * @var Auth
 */
protected $auth = null;
```

## 后台`Trait`类
我们准备了`app\admin\library\traits\Backend`Trait类，前台发送的`查询`、`添加`、`编辑`、`删除`、`排序`操作都是通过此类实现的。

## 新建后台控制器
当我们要新建一个控制器时，建议首先让它继承于`app\common\controller\Backend`类，之后，如果我们需要重写`查询`、`添加`、`编辑`等来自`Trait`类的方法，请复制`Trait`类的方法到控制器后进行重写即可。

## 前台基类
前台基类`app\common\controller\Frontend`，`api`应用下的所有控制器，都建议继承它，它已实现以下功能：
- 自动根据用户`token`完成会员的登录和鉴权。

#### 前台基类属性
新建控制器并继承于`前台基类`，可以直接重写这些属性。
``` php
/**
 * 无需登录的方法
 * 访问本控制器的此方法，无需会员登录
 */
protected $noNeedLogin = [];

/**
 * 无需鉴权的方法
 */
protected $noNeedPermission = [];

/**
 * 权限类实例
 * @var Auth
 */
protected $auth = null;
```

## 前后台权限类实例
在继承于`前台或后台`的基类之后，当前类会继承到一个权限类实例，我们可以通过它获取到管理员或会员的个人资料等信息，登录的是管理员还是会员，取决与继承于`前台`还是`后台`。
::: warning
若有编程修改会员余额的需要，请勿直接操作会员余额字段，请通过`app\admin\model\UserMoneyLog`用户余额日志模型，直接添加一条余额日志，会员余额会自动增减。
:::
``` php
// 管理员或会员是否登录
$this->auth->isLogin();

// 获取会员数据模型
$this->auth->getUser();

// 获取管理员数据模型
$this->auth->getAdmin();

// 获取管理员或会员登录token
$this->auth->getToken();

// 获取管理员或会员刷新token
$this->auth->getRefreshToken();

// 获取管理员或会员刷新token
$this->auth->getRefreshToken();

// 获取管理员信息-只输出允许输出的字段
$this->auth->getInfo();

// 获取会员信息-只输出允许输出的字段
$this->auth->getUserInfo();

// 是否是超管，拥有后台所有权限节点
$this->auth->isSuperAdmin();

// 是否是拥有所有会员中心权限的会员
$this->auth->isSuperUser();
```