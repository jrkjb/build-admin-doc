---
pageClass: max-content
---

# 配置

> 阅读此文档，建议对`ThinkPHP`有基本的了解，[TP配置文档](https://www.kancloud.cn/manual/thinkphp6_0/1037484)

## 系统配置
::: tip
我们基于TP的目录结构，在`/config/`目录下放置了`buildadmin.php`文件，该文件为`BuildAdmin`的系统配置文件。
:::

您可以在该文件内配置：
- 允许跨域访问的域名
- WEB终端允许执行的命令
- 是否开启前台会员中心
- 管理员登录重试次数
- 当拖拽排序两条记录权重相等时的处理方式
- TOKEN 驱动
- ....

#### 获取系统配置
首先需要在类文件内引入：
``` php
use think\facade\Config;
```

然后就可以使用如下方式获取配置：
``` php
// 获取版本号
Config::get('buildadmin.version');

// 获取所有系统配置
Config::get('buildadmin');

// 判断是否存在某个配置
Config::has('template');

// 参数批量设置
Config::set(['name1' => 'value1', 'name2' => 'value2'], 'config');
```

也可以使用`TP`助手函数（无需`use`）：
``` php
// 获取版本号
config('buildadmin.version');
```

请注意获取和设置配置时，不仅仅可以获取`buildadmin.php`文件内的配置，实际上它可以获取`/config/`目录下的所有配置。

## 站点配置
在站点后台中：`常规管理->系统配置`可以进行配置的项目，我们称之为站点配置。
- 站点配置是保存在数据库中的。
- 站点配置可以在后台可视化的进行修改。
- 可以随时在后台添加新的站点配置。
- 管理员添加的配置项，也可以直接在后台删除。

#### 获取站点配置
我们封装了全局公共函数`get_sys_config()`用于获取站点配置。
``` php
// 获取所有的站点配置
get_sys_config();

// 获取某一项站点配置，如下为获取站点的名称
get_sys_config('site_name');

// 获取某一个分组的站点配置
get_sys_config('', 'mail');


// 获取多个站点配置时，支持返回简洁的数据（仅返回配置键值对）
get_sys_config('', 'mail', true);
get_sys_config('', '', true);
```

## 环境配置
可以在站点根目录建立一个`.env`环境变量文件（生产环境建议忽略），我们已经在根目录提供了一个`.env-example`文件，重命名即可生效。

若要开启调试，必需在环境配置中进行配置，`.env`内容如下即可开启：
``` ini
APP_DEBUG =  true
```

环境变量不支持数组格式，如果需要使用数组，可以：
``` ini
[DATABASE]
USERNAME =  root
PASSWORD =  123456
```

如果要设置没有键的数组，可以：
``` ini
DATABASE[] =  root
DATABASE[] =  123456
```

#### 获取环境配置
环境变量的获取，不区分大小写。\
要使用`Env`类，必须先引入`think\facade\Env`。
``` php
// 获取`APP_DEBUG`，默认值 false
Env::get('APP_DEBUG', false);

// 获取`database.username`，默认值 root
Env::get('database.username', 'root');
```

更多使用方法，请参考[TP官方文档](https://www.kancloud.cn/manual/thinkphp6_0/1037484)