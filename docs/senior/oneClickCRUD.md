---
pageClass: max-content
---

# CRUD代码生成

::: tip 简介
`CRUD`是增加(Create)、检索(Retrieve)、更新(Update)和删除(Delete)几个单词的首字母简写，`BuildAdmin`实现了一键生成CRUD代码的功能，请参考下方介绍使用。
:::

## 准备工作
1. 建立好数据表，[数据表设计规范参考](https://wonderful-code.gitee.io/senior/databaseSpecification.html)

## 快速体验
``` bash
# 生成 test_build 表的CRUD代码，同时生成后台菜单
php think crud -t test_build
```
接下来：
- **生产环境**，请在内置WEB终端执行`重新发布`命令（使用内置终端请先[启动安装服务](https://wonderful-code.gitee.io/guide/install/start.html#启动安装服务)，后续我们会将一键CRUD整合进WEB终端）。
- **开发环境**(在`web`目录运行`npm run dev`)，`crud`命令执行完成后，页面会自动刷新，你可以在后台看到生成的功能了。

## 常用命令
``` bash
# 生成 test_build 表的CRUD，同时生成后台菜单（权限节点）
php think crud -t test_build

# 删除 test_build 表生成的CRUD代码
php think crud -t test_build -d 1

# 生成 test_build 表的CRUD且控制器、视图等代码文件生成在指定目录下（指定控制器名称）
php think crud -t test_build -c test/build
php think crud -t test_build -c mysite/build
php think crud -t test_build -c mysiteBuild

# 删除生成在指定目录下的 test_build 表的CRUD代码
php think crud -t test_build -c mysite/build -d 1

# 生成 test_build 表的CRUD且对应的模型名为 testmodel（指定模型名称）
php think crud -t test_build -m testmodel
php think crud -t test_build -m test/model

# 生成 test_build 表的CRUD，关联 user 表，外键为user_id主键为id 
php think crud -t test_build -r user -k user_id -p id

# 生成 test_build 表的CRUD，关联 user 表，关联 admin 表，并分别指定主键和外键
php think crud -t test_build -r user -k user_id -p id -r admin -k admin_id -p id

# 生成 test_build 表的CRUD，且将所有以 image 和 img 结尾的字段识别为图片上传组件
php think crud -t test --imagefieldsuffix=image --imagefieldsuffix=img
```

## 常用参数
``` bash
-t  --table                 表名，带不带数据表前缀均可，必填

# 以下参数均为可选的
-c  --controller            自定义控制器名
-m  --model                 自定义模型名
-f  --fields                显示的字段，默认全部字段 (array)
-o  --commonmodel           是否将模型生成在common模块下 (bool)
-r  --relation              关联模型表名，带不带数据表前缀均可 (multi)
-e  --relationmodel         生成的关联模型名，默认根据关联表解析 (multi)
-k  --relationforeignkey    关联表外键，默认使用 模型_id (multi)
-p  --relationprimarykey    关联表主键，默认读取表主键 (multi)
-l  --relationfields        关联模型显示的字段，默认全部 (multi)
-a  --relationmode          关联模式,hasone或belongsto，默认 belongsto (multi)
-s  --remoteselectfield     关联表远程select组件的显示字段名，默认 name (multi)
-d  --delete                删除之前生成的CRUD文件 (bool)
--db                        多数据库支持，此处填写`config/database/connections`中配置的数组 key，默认 mysql
--quicksearchfield          表格的快速搜索字段设置，默认'id' (array)
--sortfield                 表格默认排序字段设置，默认值为'weigh,desc'或'主键,desc'，只可以设置一个字段
--ignorefields              要排除的字段 (array)
--force                     是否为覆盖模式，非覆盖模式文件冲突时会有提示 (bool)
```

## 后缀识别参数
``` bash
--radiofieldsuffix              自动生成单选框的字段名称后缀 (array)
--checkboxfieldsuffix           自动生成 复选框 的字段名称后缀 (array)
--datetimefieldsuffix           生成 时间日期 字段名称后缀 (array)
--switchfieldsuffix             生成 开关 字段后缀 (array)
--editorfieldsuffix             富文本编辑器 后缀 (array)
--textareafieldsuffix           Textarea (array)
--cityfieldsuffix               城市选择器 (array)
--remoteselectfieldsuffix       远程select (array)
--arrayfieldsuffix              Array输入组件 (array)
--imagefieldsuffix              图片上传 (array)
--imagesfieldsuffix             图片上传(多图) (array)
--filefieldsuffix               文件上传 (array)
--filesfieldsuffix              文件上传(多文件) (array)
--numberfieldsuffix             数字输入框 (array)
--selectfieldsuffix             select(单选) (array)
--selectsfieldsuffix            select(多选) (array)
--iconfieldsuffix               icon选择器 (array)
```

## 参数输入方法
参数有多种类型，此处对各种类型的参数的输入方法进行解释

#### (array): 表示接受数组类型的参数，输入方式有两种，示例如下：
``` bash
php think crud -t test_build --fields=id,title,views,price
php think crud -t test_build --fields=id --fields=title --fields=views --fields=price
```
#### (bool): 表示接受布尔值类型的参数，示例如下：
``` bash
php think crud -t test_build -o 1
php think crud -t test_build -o 0

# 为1时，代表真，执行对应参数的逻辑
# 为0时，代表假，通常程序会忽略此参数
```
#### (multi): 关联表相关的参数使用了此类型，表示可以分开的多次输入：
``` bash
php think crud -t test_build -r user -k user_id -p id -r admin -k admin_id -p id

# 其中的 -r -p -k 就是(multi)类型的参数，它们在以上命令分别出现了两次
# 这看起来有点像(array)，但是，这类参数不支持使用','号分隔多个值
```

#### 无类型注释的参数，输入字符串类型的值即可：
``` bash
php think crud -t test_build
```
## 表格渲染方案
下表描述了表格字段的渲染规则\
三个要求中的一个满足，则应用`渲染`、`宽度`、`排序`等

|字段名要求|字段输入框类型要求|字段后缀要求|渲染为|宽度|排序|隐藏|搜索配置|
|:----:|:----:|:----:|:----:|:----:|:----:|:----:|:----:|
|id||||70|是||范围搜索|
|weigh|||||是||禁用|
||number||||||范围搜索|
||datetime||时间日期|160|是||范围搜索|
||switch||开关|||||
||image|avatar|单图|||||
||images|avatars|多图|||||
||icon||图标|||||
||url||url|||||
||radio/select|flag|标签|||||
||checkbox/selects|flags|多个标签|||||
||textarea/editor/file/files/array|||||是||

另外，不同的输入框类型，可能会有默认的`搜索配置`，比如开关，会生成一个`开`和`关`的单选`select`

## 常见问题
#### 关联表的远程select网络错误
1. 找到刚刚生成后台功能对应的`popupForm.vue`文件
2. 找到关联字段的远程select组件，该组件`type="remoteSelect"`
3. 参考以下代码的注释对远程select的参数进行调整
``` vue
import { authGroup } from '/@/api/controllerUrls'

<FormItem
    :label="t('auth.admin.grouping')"
    v-model="baTable.form.items!.group_arr"
    type="remoteSelect"
    :input-attr="{
        // 确定此字段为关联表的主键，作为select选项的value
        pk: 'id',
        // 确定此字段为关联表的字段之一，作为select选项的label
        field: 'name',
        // 确定此字段为关联表对应的控制器的`index`方法的Url，比如admin表的控制器URL为`index.php/admin/auth.admin/index`
        // 若生成过关联表的CRUD代码，则该URL地址可以从`/@/api/controllerUrls`导入使用，如下面的 authGroup
        // 若生成过关联表的CRUD代码，则该URL地址等于该表后台管理的`查看`操作请求的URL
        'remote-url': authGroup + 'index',
        placeholder: t('Click Select'),
    }"
/>
```
4. 为什么此字段有时不能自动填充？您生成crud的命令可能为`-t aaa -c aaa/ccc/dddd -m eee/ffff`，此时您再生成`-t goods -r aaa`，我们暂时不易确定`aaa`表控制器所在的位置，因此需要您自行进行填充，但后续我们将持续完善CRUD以期可以解决此问题。

#### 生成远程select多选，但提示主表需要`_id`字段
请指定`-k`参数即可