---
pageClass: max-content
---

# 表单验证

我们基于`el-form`的表单验证，额外的预设了一些列常用表单验证函数。

## 快速创建验证规则

您可以直接使用`buildValidatorData()`函数快速的构建表单验证规则，在使用前，您需要对`element plus`的[表单验证](https://element-plus.gitee.io/zh-CN/component/form.html#表单校验)有所了解。

``` ts
buildValidatorData(ruleName: string, title: string = '', trigger: string = 'blur', message: string = '')
```
#### 参数解释
|参数名|注释|
|:----:|----|
|ruleName|规则名，[可用规则](/senior/web/formValidation.html#%E5%8F%AF%E7%94%A8%E9%AA%8C%E8%AF%81%E8%A7%84%E5%88%99)|
|title|验证项标题|
|trigger|验证触发方式：`change`、`blur`|
|message|验证错误消息|

#### 示例代码
``` vue
<template>
    <!-- 一定要设置好`rules`和`model`属性，以及formItem的`prop`属性 -->
    <el-form ref="formRef" :rules="rules" :model="ruleForm" label-width="120px">
        <FormItem label="变量名" type="string" v-model="ruleForm.name" prop="name" />
        <FormItem label="变量分组" type="select" v-model="ruleForm.group" prop="group" :data="{ content: {a: '分组1', b: '分组2'} }" />
        <FormItem label="描述" type="string" v-model="ruleForm.desc" prop="desc" />
        <el-button v-blur @click="onSubmit(formRef)" type="primary"> 提交 </el-button>
    </el-form>
</template>

<script setup lang="ts">
import { reactive, ref } from 'vue'
import FormItem from '/@/components/formItem/index.vue'
import type { FormInstance, FormRules } from 'element-plus'
import { buildValidatorData } from '/@/utils/validate';

const formRef = ref<FormInstance>()
const ruleForm = reactive({
    name: '',
    group: '',
    desc: '',
})

// 此处直接使用 buildValidatorData 生成了表单验证规则
const rules = reactive<FormRules>({
    name: [buildValidatorData('required', '变量名'), buildValidatorData('varName')],
    group: [buildValidatorData('required', '', 'change', '请选择分组')],
})

const onSubmit = (formEl: FormInstance | undefined) => {
    if (!formEl) return
    formEl.validate((valid) => {
        if (valid) {
            // 验证通过
        }
    })
}
</script>
```

## 可用验证规则

可以通过`buildValidatorData()`创建的验证规则。

#### 常用规则
``` ts
buildValidatorData('required', '必填') // 请输入必填
buildValidatorData('required', '必填', 'blur', '标题是必填的') // 标题是必填的

buildValidatorData('number', '数字') // 请输入正确的数字
buildValidatorData('number', '数字', 'change', '您输入的数字不正确') // 您输入的数字不正确

buildValidatorData('integer', '整数') // 请输入正确的整数
buildValidatorData('float', '浮点数') // 请输入正确的浮点数
buildValidatorData('date', '日期') // 请输入正确的日期
buildValidatorData('url', 'URL') // 请输入正确的URL
buildValidatorData('email', 'Email') // 请输入正确的Email
```

#### 预设验证规则
``` ts
buildValidatorData('mobile') // 请输入正确的手机号
buildValidatorData('account') // 请输入正确的账户
buildValidatorData('password') // 请输入正确的密码
buildValidatorData('varName') // 请输入正确的名称
buildValidatorData('editorRequired') // 内容不能为空
```
