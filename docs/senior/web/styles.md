---
pageClass: max-content
---

# CSS/SCSS 样式

在`/web/src/styles`目录下，我们定义了多个`scss`文件，几乎所有的公用样式都保存在这些文件中。
|文件|注释|
|:----:|----|
|app.scss|应用样式表（基础样式、框架全局样式）|
|base.scss|导入所有可用样式表，`main.ts`中就可以只加载它了|
|element.scss|对`element plus`原有样式的改写|
|loading.scss|`loading`相关的样式表|
|var.scss|全局`scss`变量定义|
