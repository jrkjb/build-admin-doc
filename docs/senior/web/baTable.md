---
pageClass: max-content
---

# 表格
我们封装了`baTable`类、`baTableApi`类、`表格顶部菜单`组件等，以方便快速生成和制作表格。

## baTable 类
此类代码位于：`\src\utils\baTable.ts`，我们在类里面封装好了一个表格应有的`属性`和大量`方法`，在使用时，可以直接`new baTable`或建立新类并继承`baTable`重写之后再使用。

#### 使用示例

以下为使用示例，正常情况下仅需少许代码即可实现表格，`baTable`类支持的属性和方法非常多，在代码注释和[表格所有可用属性](/senior/web/baTable.html#表格所有可用属性)介绍

``` vue
<template>
    <!-- 这里一定要绑定上 action 事件，以响应表格的各种操作 -->
    <Table ref="tableRef" @action="baTable.onTableAction" />

    <!-- 这里正常还需引入一个表单组件，以供表格打开表单等 -->
    <!-- 请参考任意后台页面的popupForm.vue组件，比如：\src\views\backend\user\rule\popupForm.vue -->
    <!-- <PopupForm /> -->
</template>

<script setup lang="ts">
import Table from '/@/components/table/index.vue' // 导入 Table 组件
import baTableClass from '/@/utils/baTable' // 导入 baTable 类
import { baTableApi } from '/@/api/common' // 导入表格api方法生成器
import { defaultOptButtons } from '/@/components/table' // 导入默认表格操作按钮数据:拖拽排序、编辑、删除按钮
import { userUser } from '/@/api/controllerUrls' // 导入user管理的控制器Url：/index.php/admin/user.user/

const tableRef = ref()

// 直接实例化 baTableClass 并传递各种参数
const baTable = new baTableClass(
    new baTableApi(userUser), // 一个api类的实例，自动生成index、edit、del、等url和请求方法，提供控制器url即可
    // 表格数据
    {
        // 表格列
        column: [
            { type: 'selection', align: 'center', operator: false },
            { label: 'ID', prop: 'id', align: 'center', operator: 'LIKE', operatorPlaceholder: '模糊查询', width: 70 },
            { label: '用户名', prop: 'username', align: 'center', operator: 'LIKE', operatorPlaceholder: '模糊查询' },
            // ...
            {
                label: '操作',
                align: 'center',
                width: '100',
                render: 'buttons',
                buttons: defaultOptButtons(['edit', 'delete']),
                operator: false,
            },
        ],
        // 不允许双击编辑的列的 prop
        dblClickNotEditColumn: [undefined],
        // ...属性很多，请参考本文下方的表格全部可用属性，或翻阅源代码（有注释）
    },
)

// 实例化表格后，将 baTable 的实例提供给上下文
provide('baTable', baTable)

// 相当于表格的onMounted，也可以在页面的onMounted时执行
baTable.mount()

// 获取数据，可以在页面的onMounted时执行，也可以比如先请求一个API，再执行获取数据
baTable.getIndex()!.then(() => {
    // 查看请求完毕（已获取到表格数据）
    // baTable.initSort() // 初始化默认排序（如果需要）
    // baTable.dragSort() // 初始化拖拽排序（如果需要）
})
</script>
```

## 深度使用表格

#### 随意设置表格属性参数
表格的参数属性，在代码任何地方，都可以进行更改，比如：
``` ts
// 您可以先设置好一些表格属性，再执行“查看”请求，表格的所有可用属性，在本文下方可以寻找到，您也可以直接阅读源代码
const example1 = () => {
    baTable.table.filter.limit = 20
    baTable.table.filter.page = 2
    baTable.table.filter!.search = [{field:'status',val:'1',operator:'=',render:'tags'}]
    baTable.getIndex()
}
```

``` ts
// 数据加载完成后打开公共搜索
baTable.getIndex()?.then(() => {
    baTable.table.showComSearch = true
})
```

#### 自动获取表格筛选条件
有时您需要带着一些筛选条件跳转到表格，并获取筛选后的数据，请直接参考会员管理->编辑会员资料->调整余额；我们在跳转到余额日志管理时，携带了`user_id`参数，该参数由于和表格字段对应，所以会自动获取到并作为公共搜索的筛选条件。

#### 自定义表格顶部或侧边按钮
有时您需要自定义表格顶部或表格侧边按钮，请参考数据安全管理->数据回收站，我们在该管理功能内，通过插槽自定义了一个表格顶部的`还原`按钮，并在实例化`baTable`时定义了一个额外的表格侧边确认还原按钮

#### 自定义单元格渲染
若预设的所有单元格渲染方案均不能满足您的需要，您可以参考以下示例，自定义渲染单元格
``` ts
const baTable = new baTableClass(
    new baTableApi(authAdmin),
    {
        column: [
            { label: 'id', prop: 'id', render: 'customTemplate', customTemplate: (row: TableRow, field: TableColumn, value: any) => {
                return '<b>' + value + '</b>';
            } },
        ]
    }
)
```
::: warning
但是，`customTemplate`可以直接渲染`HTML`的同时，也带来了安全隐患，请确保该函数返回的内容是`xss`安全的；比如您想要自定义渲染`username`字段，那么请确保该字段永远不会被用户输入html代码。
:::

#### 单元格渲染前格式化
在您使用预设的单元格渲染方案时`icon|switch|image|tag...`，可以在渲染前，通过函数对单元格值进行一次处理：
``` ts
const baTable = new baTableClass(
    new baTableApi(authAdmin),
    {
        column: [
            { label: 'id', prop: 'id', render: 'tags', renderFormatter: (row: TableRow, field: TableColumn, value: any) => {
                return value + ' - 为该列所有值，加了个后缀';
            } },
        ]
    }
)
```


## 表格所有可用属性
``` ts
const baTable = new baTableClass(api,
    // 表格参数
    {
        // 表格的ref
        ref: undefined,
        // 数据表主键字段
        pk: 'id',
        // 数据源，通过api自动加载
        data: [],
        // 路由remark（显示到表格顶部的文案）
        remark: null,
        // 表格加载状态
        loading: false,
        // 是否展开所有子项
        expandAll: false,
        // 选中项
        selection: [],
        // 不需要'双击编辑'的字段
        dblClickNotEditColumn: [undefined],
        // 列数据
        column: [],
        // 数据总量
        total: 0,
        // 字段搜索,快速搜索,分页等数据
        filter: {
            limit: 10,
            page: 1,
            order: 'createtime',
            // 快速搜索，指定字段请在对应后端控制器内定义，默认为id
            quick_search: '快速搜索关键词',
            // 公共搜索
            search: [
                {
                    'field': 'title',
                    'val': '搜索标题字段',
                    'operator': 'LIKE',
                    'render': 'tags',
                }
            ]
        },
        // 拖动排序限位字段:例如拖动行pid=1,那么拖动目的行pid也需要为1
        dragSortLimitField: 'pid',
        // 接受url的query参数并自动触发通用搜索
        acceptQuery: true,
        // 显示公共搜索
        showComSearch: false,
        // 扩展数据，预设的随意定义字段，整个baTable通常会暴露到上下文，定义后可在上下文中使用此属性
        extend: {},
    },
    // 表单参数
    {
        // 表单ref，new时无需传递
        ref: undefined,
        // 表单label宽度
        labelWidth: 160,
        // 当前操作:add=添加,edit=编辑
        operate: '',
        // 被操作数据ID,支持批量编辑:add=[0],edit=[1,2,n]
        operateIds: [],
        // 表单数据，编辑/添加时的表单字段当前输入和预加载的数据
        items: {},
        // 提交按钮状态
        submitLoading: false,
        // 默认表单数据(添加)
        defaultItems: {
            username: '默认用户名',
            nickname: '默认昵称',
        },
        // 表单字段加载状态
        loading: false,
        // 扩展数据，预设的随意定义字段，整个baTable通常会暴露到上下文，定义后可在上下文中使用此属性
        extend: {},
    }, 
    // 操作前置方法列表，返回false则取消原操作
    {
        getIndex: () => {},// 查看前
        postDel: ({ids: string[]}) => {},// 删除前
        requestEdit: ({id: string}) => {},// 编辑请求前
        onTableDblclick: ({row: TableRow, column: any}) => {},// 双击表格具体操作执行前
        toggleForm:({operate: string, operateIds: string[]}) => {},// 表单切换前
        onSubmit:({formEl: InstanceType<typeof ElForm> | undefined, operate: string, items: anyObj}) => {},// 表单提交前
        onTableAction: ({event: string, data: anyObj}) => {},// 表格内事件响应前
        onTableHeaderAction: ({event: string, data: anyObj}) => {}// 表格顶部菜单响应前
        mount: () => {},// 初始化前
    },
    // 操作后置方法列表，先执行表格本身的查看、删除、编辑、双击、等，再执行本处添加的后置操作
    {
        getIndex: ({res: ApiResponse}) => {},// 查看后
        postDel: ({res: ApiResponse}) => {},// 删除后
        requestEdit: ({res: ApiResponse}) => {},// 编辑请求后
        onTableDblclick: ({row: TableRow, column: any}) => {},// 双击表格具体操作执行后
        toggleForm:({operate: string, operateIds: string[]}) => {},// 表单切换后
        onSubmit:({res: ApiResponse}) => {},// 表单提交后
        onTableAction: ({event: string, data: anyObj}) => {},// 表格内事件响应后
        onTableHeaderAction: ({event: string, data: anyObj}) => {}// 表格顶部菜单响应后
        mount: () => {},// 初始化后
    }
)
```

## baTableApi 类
此类代码位于：`\src\api\common.ts`，它实现快速生成一个控制器的：增、删、改、查、排序的操作url和方法，提供控制器的url即可。此类通常与`baTable`类搭配使用，若需单独定义`api`方法，可以直接在`\src\api`目录下定义即可。

#### 使用示例
``` vue
<script>
import { baTableApi } from '/@/api/common' // 导入表格api方法生成器
import { userUser } from '/@/api/controllerUrls' // 导入user管理的控制器Url，类似：/index.php/admin/user.user/

let baApi = new baTableApi(userUser)
baApi.index({}) // 请求查看，参数为筛选条件，具体使用请参考baTable类
baApi.edit({}) // 请求编辑数据，参数为被编辑项id等
baApi.del({}) // 请求删除，参数为被删除数据的ids
// ...
</script>
```

## 表格顶部菜单
菜单按钮可以自动根据当前路由进行`鉴权`，当前管理员无权限的按钮，则不会显示。

#### 属性列表
|属性名|注释|
|:----:|----|
|buttons|要显示的菜单按钮数组|
|@action|绑定`baTable.onTableHeaderAction`即可|
|quick-search-placeholder|快速搜索输入框的`placeholder`|

#### 支持的菜单按钮
|菜单按钮|注释|
|:----:|----|
|refresh|刷新按钮|
|add|添加|
|edit|编辑|
|delete|删除|
|comSearch|公共搜索|
|unfold|暂开/折叠，与`baTable.table.expandAll`属性关联|

#### 示例代码
``` vue
<template>
    <!-- 一定要绑定 action 事件，以供表格响应操作-->
    <TableHeader
        :buttons="['refresh', 'add', 'edit', 'delete']"
        :quick-search-placeholder="'通过组名模糊搜索'"
        @action="baTable.onTableHeaderAction"
    />
</template>

<script>
import TableHeader from '/@/components/table/header/index.vue'
</script>
```