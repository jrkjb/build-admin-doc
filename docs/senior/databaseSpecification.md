---
pageClass: max-content
---

# 数据表设计规范

::: tip
当你使用本规范设计数据表时，搭配一键 CRUD，可以非常舒畅的完成后台功能制作
:::

## 字段类型

|                        类型                         |   备注   | CRUD 搭配点                                    |
| :-------------------------------------------------: | :------: | ---------------------------------------------- |
|                        enum                         |  枚举型  | **自动生成**单选框                                 |
|                         set                         |  set 型  | 自动生成复选框                                 |
|                        date                         |  日期型  | 自动生成日期的选择组件                         |
|                        year                         |  年份型  | 自动生成年份选择组件                           |
|                        time                         |  时间型  | 自动生成时间选择组件                           |
|                 datetime/timestamp                  | 时间日期 | 自动生成时间日期的选择组件                     |
|                decimal/double/float                 |  浮点型  | 自动生成 Number 输入框，步长根据默认值自动计算 |
|        int/bigint/mediumint/smallint/tinyint        |   整型   | 自动生成 Number 输入框，步长为 1               |
| longtext/text/mediumtext/smalltext/tinytext/bigtext |  文本型  | 自动生成 textarea 文本框                       |

## 字段名称

|    名称    |   备注   | 字段类型要求 | CRUD 搭配点                                                      |
| :--------: | :------: | :----------: | ---------------------------------------------------------------- |
|   weigh    |   权重   |     int      | 后台的排序字段，如果存在该字段将出现排序按钮，可上下拖动进行排序 |
| createtime | 创建时间 |     int      | 记录的添加时间字段，不需要手动维护                               |
| updatetime | 更新时间 |     int      | 记录的更新时间的字段，不需要手动维护                             |

## 字段后缀（名称结尾）

|        后缀         |   名称示例   |                    字段类型要求                     | CRUD 搭配点|
| :----: | :----: | :----: | ---- |
|        array        |  namearray   |                         无                          | 自动生成数组输入组件|
|  list/select/data   |   namelist   |                         无                          | 自动生成 select，单选|
| lists/selects/multi | name_selects |                         无                          | 自动生成 select，多选|
|        \_id         |   user_id    |                         无                          | 自动生成关联表远程 select，单选|
|        \_ids        |   user_ids   |                         无                          | 自动生成关联表远程 select，多选|
|        city         |     city     |                         无                          | 自动生成城市选择器|
|    image/avatar     |  desc_image  |                         无                          | 自动生成上传图片组件，单图|
|   images/avatars    |  descimages  |                         无                          | 自动生成上传图片组件，多图|
|        file         |  attachfile  |                         无                          | 自动生成上传文件组件，单文件|
|   files/file_list   | attachfiles  |                         无                          | 自动生成上传文件组件，多文件|
|        icon         |     icon     |                         无                          | 自动生成图标选择器|
|   number/int/num    |   add_num    |                         无                          | 自动生成 Number 输入框，步长根据默认值自动计算|
|    time/datetime    | refreshtime  |                         int                         | 自动创建选择时间日期的组件|
|textarea/multiline/rows| rows       |                      varchar                        |自动生成Textarea输入框|
|    status/state     |    status    |                      tinyint(1)/char(1)             |自动生成单选框|
|    switch/toggle    |  log_switch  |             tinyint(1)/int/enum/char(1)             | 自动生成开关组件，`真值`、`1`代表开，`假值`、`0`代表关 |
|   content/editor    |   content    | longtext/text/mediumtext/smalltext/tinytext/bigtext | 自动生成富文本编辑器|

## 字段注释
CRUD会将`字段注释`和`表注释`解析为`字典`。

将字段`status`注释设置为`状态:0=禁用,1=启用`，那么`一键CRUD`生成的页面如下所示。
- ![](/images/databaseSpecification/databaseSpecification1.png)
- ![](/images/databaseSpecification/databaseSpecification2.png)
``` bash
# 另外的两个参考示例
菜单类型:tab=选项卡,link=链接,iframe=Iframe
扩展属性:none=无,add_rules_only=只添加为路由,add_menu_only=只添加为菜单
```
表注释示例：
- `会员组表`将被解析为`会员组管理`
- `会员积分变动表`将被解析为`会员积分变动管理`

## 其他注意事项
1. 不支持`复合主键`，请为数据表设置`单字段主键`。